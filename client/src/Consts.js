import * as Yup from 'yup'

//Allow full name with up to 2 spaces only english chars
const fullNameRegex = new RegExp(/^[a-zA-Z]{3,9}.(\s[a-zA-Z]{3,9}){1,3}$/)

//Allow 3-255 English Chars,digits commas and periods.
const descriptionRegex = new RegExp(/^[a-zA-Z0-9 .,]{3,255}$/)

//Number is 05XXXXXXXX for example 0547038728
const phoneNumberRegex = new RegExp(/^05[0-9]{8}$/)


let columns = [
    {
        name: 'Full Name',
        selector: row => row.fullName,
        sortable: true,
    },
    {
        name: 'Phone Number',
        selector: row => row.phoneNumber,
        sortable: true,
    },
    {
        name: 'Description',
        selector: row => row.description,
        sortable: true,
    },
]

let searchViewSchema = Yup.object().shape({
    FullName: Yup.string().min(3, "At least 3 chars").max(31, "No more than 31 chars").matches(fullNameRegex, "Invalid Chars").required('Required'),
});
let addContactSchema = Yup.object().shape({
    FullName: Yup.string().min(3, "At least 3 chars").max(31, "No more than 31 chars").matches(fullNameRegex, "Invalid Chars").required('Required'),
    PhoneNumber: Yup.string().length(10, "Invalid amout of chars").matches(phoneNumberRegex).required('Required'),
    Description: Yup.string().min(3, "Description too short").max(255, 'Description too long').matches(descriptionRegex, 'Invalid chars in description').required('Required'),
});
let server = "http://"+(process.env.SERVER_ADDRS||"127.0.0.1")+":3001";
export { columns, searchViewSchema,addContactSchema,server}
