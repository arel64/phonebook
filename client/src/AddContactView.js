import React from 'react';
import { Formik } from 'formik';
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'

export default class SearchView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            validated: false,
            schema: props.schema
        };
    }
   
    async handleSubmit(values, actions) {
        console.log(actions)
        const url = `/addContact?fullName=${encodeURIComponent(values.FullName)}&phoneNumber=${encodeURIComponent(values.PhoneNumber)}&description=${encodeURIComponent(values.Description)}`
        const res = await fetch(url, {
            method :'PUT',
        });
        let statusCode = res.status
        if ((statusCode !== 200)) {
            if (statusCode === 406) {
                alert('Data Rejected')
            } else if (statusCode === 409){
                alert('Duplicate Entry, Try a different fullName')
            } else {
                alert('General Error')
            }
            return;
        } else {
            actions.resetForm()
        }

        
    }
    render() {
        return (

            <Formik
                validationSchema={this.state.schema}
                onSubmit={this.handleSubmit}
                initialValues={{
                    FullName: '',
                    PhoneNumber: '',
                    Description: '',
                }}
            >
                {({ handleSubmit, handleChange, values, errors, touched, props }) => (
                    <Form noValidate validated={this.state.validated} onSubmit={handleSubmit}>
                        <Container>
                            <Row>
                                <h3>Add Contact</h3>
                            </Row>
                            <Row className="mb-3">
                                <Form.Group as={Col} md="6" xs="12" controlId="validationFormik01">
                                    <Form.Label>FullName</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="FullName"
                                        value={values.FullName}
                                        onChange={handleChange}
                                        isInvalid={!!errors.FullName}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {errors.FullName}
                                    </Form.Control.Feedback>
                                </Form.Group>
                                <Form.Group as={Col} md="6" xs="12" controlId="validationFormik02">
                                    <Form.Label>PhoneNumber</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="PhoneNumber"
                                        value={values.PhoneNumber}
                                        onChange={handleChange}
                                        isInvalid={!!errors.PhoneNumber}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {errors.PhoneNumber}
                                    </Form.Control.Feedback>
                                </Form.Group>
                            </Row>
                            <Row className="mb-3">
                                <Form.Group as={Col} controlId="validationFormik03">
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control
                                        type="text"
                                        as="textarea"
                                        placeholder="Description"
                                        name="Description"
                                        value={values.Description}
                                        onChange={handleChange}
                                        isInvalid={!!errors.Description}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {errors.description}
                                    </Form.Control.Feedback>
                                </Form.Group>
                            </Row>
                            <Button type="submit">Submit form</Button>
                        </Container>

                    </Form>
                )}
            </Formik>
        );
    }
} 