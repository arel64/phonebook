import React from 'react';
import DataTable from 'react-data-table-component';
import socketIOClient from "socket.io-client";
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import { Formik } from 'formik';




export default class SearchView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lastSearched:'',
            tableData:'',
            validated: false,
            columns: props.columns,
            server:props.server,
            schema:props.schema,
            fullNameRegex: props.fullNameRegex
        }
    }
    handleSubmit = async(value,action) => {
        
        const res = await fetch(`/searchContacts?fullName=${encodeURIComponent(value.FullName)}`);
        let statusCode = res.status
        if ((statusCode !== 200)) {
            if (statusCode === 406) {
                alert('Invalid Full Name!')
            } else {
                alert('General Error')
            }
            return;
        }
        
        const resData = await res.json()
        /**
         * Update Table data and make sure lastSearched is up to date 
         */
        this.setState({
            tableData: resData,
            lastSearched: value.FullName
        })

    }
    async componentDidMount() {
        const socket = socketIOClient(this.state.server);
        socket.on("contactAdded", data => {
            console.log(data);
            if (this.state.lastSearched.localeCompare(data.fullName)===0)
            this.setState({ tableData: [...this.state.tableData, data] })
        });
    }
    render() {
        return (
            <div>
                <Formik
                    validationSchema={this.state.schema}
                    onSubmit={this.handleSubmit}
                    initialValues={{
                        FullName: '',
                    }}
                >
                    {({ handleSubmit, handleChange, values, errors, touched, props }) => (
                        <Form noValidate validated={this.state.validated} onSubmit={handleSubmit}>
                            <Container>
                                <Row>
                                    <h3>Search Contact By Name</h3>
                                </Row>
                                <Row className="mb-3">
                                    <Form.Group as={Col} controlId="FullNameSearch">
                                        <Form.Label>FullName</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="FullName"
                                            name="FullName"
                                            value={values.FullName}
                                            onChange={handleChange}
                                            isInvalid={!!errors.FullName}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {errors.FullName}
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                </Row>
                                <Button type="submit">Search</Button>
                            </Container>

                        </Form>
                    )}
                </Formik>
                <DataTable
                    columns={this.state.columns}
                    data={this.state.tableData}
                    striped={true}>
                </DataTable>
            </div>

            
        );
    }
}