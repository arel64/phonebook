
import socketIOClient from "socket.io-client";
import DataTable from 'react-data-table-component';
import React from 'react';

class ViewContacts extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: [],
            columns: props.columns,
            server: props.server
        };
    }

    async componentDidMount() {
        const res = await fetch('/viewContacts');
        const data = await res.json();
        this.setState({ tableData: data });
        const socket = socketIOClient(this.state.server);
        socket.on("connection", data => {
            console.log(data);
        });
        socket.on("contactAdded", data => {
            console.log(data);
            this.setState({ tableData: [...this.state.tableData, data] })
        });
    }

    render() {
        return (
            <div>
                <h3>All Contacts</h3>
                <DataTable
                    columns={this.state.columns}
                    data={this.state.tableData}
                    striped={true}></DataTable>
            </div>
        );
    }
}

export default ViewContacts;
