import React from 'react';
import SearchView from './SearchView';
import ViewContacts from './ViewContacts.js';
import AddContactView from './AddContactView';
import { columns, server, searchViewSchema,addContactSchema } from './Consts'
import Container from 'react-bootstrap/Container'
import 'bootstrap/dist/css/bootstrap.min.css';


 
class App extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      tableData: [],
    };
  }

  

  render() {
    return (
      <Container>
        <ViewContacts
          columns={columns}
          server={server}
        >
        </ViewContacts>
        <SearchView
          columns={columns}
          server={server}
          schema={searchViewSchema}
        ></SearchView>
        <AddContactView
          schema={addContactSchema}
        ></AddContactView>
      </Container>
        
    );
  }
}

export default App;
