FROM node:14

WORKDIR /usr/src/app

RUN ls -al

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3001

CMD [ "node", "server.js" ]