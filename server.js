require('dotenv').config()
const express = require('express'); // importing Express

const router = require('./app/src/routes/router.js')
const { socketConnection } = require('./app/src/models/socket')

const port = process.env.PORT || 3001; // Defining port by environmental parameter or 3001

const app = express(); // instance of express
app.use('/', router)

const http = require("http")
const server = http.createServer(app)

socketConnection(server)
// App is listening to user-defined port or 300 and printing the information to the console.


server.listen(port, function (req, res) {
    console.log(`Server is listening on port ${port}!`)
})