const { check, oneOf, validationResult } = require('express-validator');


/**
 * Regex Used
 */

//Allow full name with up to 2 spaces only english chars
const fullNameRegex = /^[a-zA-Z]{3,9}.(\s[a-zA-Z]{3,9}){1,3}$/

//Allow 3-255 English Chars,digits commas and periods.
const descriptionRegex = /^[a-zA-Z0-9 .,]{3,255}$/

//Number is 05XXXXXXXX for example 0547038728
const phoneNumberRegex = /^05[0-9]{8}$/

/**
 *  The following are verifications for routes, add verification as you wish
 *  The handeling of results must occur seperatly 
 */


exports.viewContacts = oneOf([

    check('limit').escape().isInt({ min: 0, max: 2147483646, allow_leading_zeroes: false }),
    check('limit').escape().isEmpty()
]);

exports.searchContacts = [
    check('fullName').escape().notEmpty().matches(fullNameRegex),
];

exports.addContact = [
    check('phoneNumber').escape().notEmpty().matches(phoneNumberRegex),
    check('fullName').escape().notEmpty().matches(fullNameRegex),
    check('description').escape().notEmpty().matches(descriptionRegex)
];
