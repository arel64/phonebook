const {  validationResult } = require('express-validator');
exports.checkForErrors = (req,res,next)  => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(406).json({ errors: errors.array() });
    }
    next()
}
