require('dotenv').config()
const express = require('express');
const connection = require('../models/mysql.js');
const contact = require('../models/contact.js');

describe('Unit Test Init', function () {
    let app,
        date;

    // Called once before any of the tests in this block begin.
    before(function (done) {
        app = express();
        // Any asynchronous action with a callback.
        app.listen(3000, function (err) {
            if (err) { return done(err); } else {
                return done();
            }
            
        });
    });

    // Called once before each of the tests in this block.
    beforeEach(function () {
        date = new Date();
    });

    // Called after all of the tests in this block complete.
    after(function () {
        console.log("Our applicationa tests done!");
    });

    // Called once after each of the tests in this block.
    afterEach(function () {
        console.log("The date for that one was", date);
    });

    it('testCorrectInit', function () {
        if (connection === null) {
            throw new Error("Connection Invalid");
        }
    });
    it('testContactViewNoLimit', function () {
        contact.fetchUsers()
        .then(value => {
           return done()
        }).catch(err => {
            return done(err)
        })
    });
    it('testContactViewNoLimit', function () {
        contact.fetchUsers(3)
        .then(value => {
           return done()
        }).catch(err => {
            return done(err)
        })
    });
    it('testContactAddValid', function () {
        contact.addContact('05' + Math.floor(Math.random() * 100000000) + 1,'Arel Sharon','Candidate for an interview.')
        .then(value => {
           return done()
        }).catch(err => {
            return done(err)
        })
    });
    it('testContactAddDuplicate', function () {
        let randPhoneNumber = '05' + Math.floor(Math.random() * 100000000) + 1
        contact.addContact(randPhoneNumber,'Arel Sharon','Candidate for an interview.')
        contact.addContact(randPhoneNumber,'John Joe','Another,Worse, Candidate for an interview.')
        .then(value => {
           return done("Shouldn't have let duplicate phonenumbers to pass!")
        }).catch(err => {
            if (err.errno === 1062) {
                //Duplicate entry!
                return done()
            }
            return done(err)
        })
    });
});