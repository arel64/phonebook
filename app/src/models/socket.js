let io;
exports.socketConnection = (server) => {
    io = require('socket.io')(server, {
        /**
         * Prevent cross origin errors between dockers,
         * Should, In production enviornment be modified to client domain
         */
        cors: {
            origin: '*',
        }
    });
    io.on("connection", (socket) => {
        console.log("Connected!")
        socket.emit("connection", "socketConnected!")
    })
    /**
     * This will emit to the client that a contact was added along with the data
     */
    io.on("contactAdded", (socket) => {
        console.log("contactAdded")
        socket.emit("contactAdded", userJson)
    })
};

exports.contactAdded = (userJson) => io.emit("contactAdded",userJson);
