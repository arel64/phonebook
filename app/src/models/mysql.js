
/**
 * This module will generate mysql tables if not existant in enviorment,
 * Removing the responsebilty from docker and making struct adjustments easy
 */

const mysql = require('mysql2')
let connection = mysql.createConnection({
    host: process.env.MYSQL_HOST,//Retrived from mysql docker hostname in docker-compose
    user: process.env.SQLUSER,
    password: process.env.SQLPASS,
    database: process.env.DBNAME,
    port: 3306
})
/**
 * Create all missing tables, connection will be null upon error
 */
const init = async () => {
    try {
        connection.connect()
        const contacts = `CREATE TABLE IF NOT EXISTS contacts(
            id int(6) NOT NULL AUTO_INCREMENT,
            phoneNumber varchar(10) NOT NULL,
            fullName varchar(31) NOT NULL,
            description varchar(255) NOT NULL,
            creationDatetime timestamp NOT NULL DEFAULT current_timestamp(),
            PRIMARY KEY(id),
            UNIQUE KEY phoneNumber(phoneNumber)
        ) ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT = 'Contacts Table';`;
        connection.query(contacts)
    } catch {
        connection = null
    }
}
init()
module.exports.connection = connection
