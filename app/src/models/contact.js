const connection = require('./mysql')

/**
 * private function that retrives contact information by field and value restriced by limit
 * @param {int} limit max rows (optional)
 * @param {string} field to filter results by (optional)
 * @param {any}  value value of field of which is needed (optional)
 * @returns Promise rejects on server error, resolves on 0 or more returned values
 */
const _fetchUsers = async (limit = null, field = true, value = true) => {
    let query
    if (limit === null) {
        query = 'SELECT id,phoneNumber,fullName,description FROM contacts WHERE '+field+'=?'
    } else {
        query = 'SELECT id,phoneNumber,fullName,description FROM contacts WHERE '+field+'=? LIMIT ' + limit
    }
    
    return new Promise((resolve, reject) => {
        connection.connection.query(query,value,function (err, results) {
            if (err) { return reject(err) }
            resolve(results)
        })
    })
}
/**
 * private function that Will add Contact to DB
 * @param {string} phoneNumber phone num of new contact (with leading 0)  
 * @param {string} fullName full name of new contact
 * @param {string} description  description of new contact
 * @returns Promise that will resolve if added and reject if contact isn't added(Due to Error or duplicate)
 */
const _addContact = async (phoneNumber, fullName, description) => {
    const query = 'INSERT INTO contacts(phoneNumber, fullName, description) VALUES (?,?,?)'
    const values = [phoneNumber, fullName, description]
    return new Promise((resolve, reject) => {
        connection.connection.query(query, values, function (err, results) {
            if (err) { return reject(err) }
            resolve(results.insertId)
        })
    })
}
/**
 * Will retrieve all contacts without filtering
 * @param {int} limit how many contacts to fetch 
 * @returns promise that will resolve to array of all contacts
 */
const fetchUsers = async (limit) => {
    return _fetchUsers(limit)
}
/**
 * Searches for contacts by thier fullname
 * note that since fullname is not unique more than one (or zero) contacts might return
 * @param {string} fullName 
 * @returns promise that will resolve to array of all contacts fitting the fullname criteria exactly
 */
const searchContactsByName = async (fullName) => {
    return _fetchUsers(null,'fullName',fullName)
}

/**
 * Will add Contact to DB
 * @param {string} phoneNumber phone num of new contact (with leading 0)
 * @param {string} fullName full name of new contact
 * @param {string} description  description of new contact
 * @returns Promise that will resolve if added and reject if contact isn't added(Due to Error or duplicate)
 */
const addContact = async (phoneNumber, fullName, description) => {
    return _addContact(phoneNumber, fullName, description)

}
module.exports = { fetchUsers,searchContactsByName,addContact}
