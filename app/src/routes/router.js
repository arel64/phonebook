const express = require('express');
const contact = require('../models/contact'); 
const socket = require('../models/socket'); 
const { viewContacts, searchContacts,addContact} = require('../middleware/validator');
const {checkForErrors} = require('../middleware/validationErrorHandle'); 
const router = express.Router(); 

router.get('/viewContacts', viewContacts, checkForErrors, function (req, res) {
    
    //Get limit, validated integer
    let limit = req.query.limit

    /**
     * Fetch Users according to limit ,if found anyamount of users (Including 0) retrun jsonified array
     */
    contactsPromise = contact.fetchUsers(limit).
        then(value => {
            return res.status(200).json(value)
        }).catch(err => {
            console.log(err)
            /**
             * Unexpected Error
             */
            return res.status(500).end()
        })
})
router.get('/searchContacts', searchContacts, checkForErrors, function (req, res) {

    //Get Fullname, validated string
    let fullName = req.query.fullName

    /**
     * Fetch Users according to Fullname ,if found any amount of users (Including 0) retrun jsonified array
     */
    contactsPromise = contact.searchContactsByName(fullName).
        then(value => {
            if (!value) {
                //Not found
                return res.status(404).end()
            }
            return res.status(200).json(value)
        }).catch(err => {
            console.log(err)
            /**
             * Unexpected Error
             */
            return res.status(500).end()
        })
})
router.put('/addContact', addContact, checkForErrors, function (req, res) {
   
    //store validated query params

    let phoneNumber = req.query.phoneNumber
    let fullName    = req.query.fullName
    let description = req.query.description
    
    /**
     * Add contact to phonebook, all fields mandatory
     */
    contactsPromise = contact.addContact(phoneNumber,fullName,description).
        then(value => {
            //Contact Added!
            socket.contactAdded({
                id: value,
                phoneNumber,
                fullName,
                description
            })
            return res.status(200).end()
        }).catch(err => {
            if (err.errno === 1062) {
                //Notify the user that he attempted to enter another contact with the same phone number
                return res.status(409).end('Duplicate Entry!')
            }
            //General Error
            console.log(err)
            return res.status(500).end()
        })
})
module.exports = router;